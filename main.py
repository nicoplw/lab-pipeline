import pytest

class Main:
    def __init__(self, name):
        self.name = name

    def intro(self):
        print("""\033[1;31;40m
        ██████  ██████   ██████  ██████   █████  ███    ██ ██████   ██████  
        ██   ██ ██   ██ ██    ██ ██   ██ ██   ██ ████   ██ ██   ██ ██    ██ 
        ██████  ██████  ██    ██ ██████  ███████ ██ ██  ██ ██   ██ ██    ██ 
        ██      ██   ██ ██    ██ ██   ██ ██   ██ ██  ██ ██ ██   ██ ██    ██ 
        ██      ██   ██  ██████  ██████  ██   ██ ██   ████ ██████   ██████
        \033[0;31;40m \n""")        
        print("        © "+ self.name)

        return self.name

m = Main("Nico")
m.intro()