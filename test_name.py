import pytest
from main import *

m = Main("Nico") 

# will run 3 times since there are 3 parameters
@pytest.mark.parametrize("nameTest",
                            [
                                # ("Paul Atreides"),
                                # ("Chani"),
                                ("Nico") # this is going to pass
                            ]
                        )
def test_name(nameTest):
    name = m.intro()
    assert name == nameTest